# UNO

![Unser Logo](Logo_Uno.png)
## Was ist unser Ziel
Unser Ziel ist es ein Uno Spiel zu programmieren. Dieses soll man  mit einem NPC zusammen spielen können. Es soll alle Karten des Normalen UNOs enthalten. Es hat unbegrenzt Karten, so wird das Spiel erst beendet wenn jemand gewonnen hat.

##  Eingesetzte API
Für dieses Projekt brauchten wir die [Deck of Cards](http://deckofcardsapi.com/) und die [Juno](https://github.com/dougtebay/JUNO) Applikationen

## Teammitglieder
Zu unserem Team gehört:  
Max  
Joel  
Elia

## Wie kann man zu unserem Projekt beitragen
Man kann uns helfen in dem man Fehler behebt oder Verbesserungs vorschläge bringt.

## Lizenz
Unser Projekt ist mit der MIT-Lizenz lizenziert. Für mehr Informationen gehen sie zur Datei [LICENSE](LICENSE)